<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="namadepan"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="namablkng"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gndr" value="ml">Male <br>
        <input type="radio" name="gndr" value="fm">Female <br>
        <input type="radio" name="gndr" value="ot">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="ns">
            <option value="1">Indonesian</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bhs"> Bahasa Indonesia <br>
        <input type="checkbox" name="bhs"> English <br>
        <input type="checkbox" name="bhs"> Other <br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>


        <input type="submit" value="Sign Up">
    </form>

</body>
</html>